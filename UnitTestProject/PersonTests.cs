﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AshTree;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AshTree.Tests
{
    [TestClass()]
    public class PersonTests
    {
        [TestMethod()]
        public void AddChildTest()
        {
            Person ivan = new Person("Иван Иванович", Genders.Male, 1970);

            Person anastasya = new Person("Анастасия Ивановна", Genders.Female, 1990);
            ivan.AddChild(anastasya);
            Assert.AreEqual(anastasya.Parent, ivan);
            Assert.IsTrue(ivan.Children.Count == 1);
            Assert.AreEqual(ivan.Children[0], anastasya);
        }

        [TestMethod()]
        public void FindRelativesTest()
        {
            Person ivan = new Person("Иван Иванович", Genders.Male, 1970);
                Person anastasya = new Person("Анастасия Ивановна", Genders.Female, 1990);
                Person victor = new Person("Виктор Иванович", Genders.Male, 1990);
                    Person boris = new Person("Борис Викторович", Genders.Male, 2000);
                Person peter = new Person("Петр Иванович", Genders.Male, 1991);
                    Person vlad = new Person("Владимир Петрович", Genders.Male, 2001);
            ivan.AddChild(anastasya);
            ivan.AddChild(victor);
            victor.AddChild(boris);
            ivan.AddChild(peter);
            peter.AddChild(vlad);

            List<Person> relatives = vlad.FindRelatives(Genders.Male, 1).ToList();
            Assert.IsTrue(relatives.Count == 0);

            relatives = vlad.FindRelatives(Genders.Male, 2).ToList();
            Assert.IsTrue(relatives.Count == 1);
            Assert.AreEqual(boris, relatives[0]);

            relatives = vlad.FindRelatives(Genders.Female, 2).ToList();
            Assert.IsTrue(relatives.Count == 0);
        }

        [TestMethod()]
        public void FindDescendantsRecursiveTest()
        {
            Person ivan = new Person("Иван Иванович", Genders.Male, 1970);
            Person anastasya = new Person("Анастасия Ивановна", Genders.Female, 1990);
            Person victor = new Person("Виктор Иванович", Genders.Male, 1990);
            Person boris = new Person("Борис Викторович", Genders.Male, 2000);
            Person peter = new Person("Петр Иванович", Genders.Male, 1991);
            Person vlad = new Person("Владимир Петрович", Genders.Male, 2001);
            ivan.AddChild(anastasya);
            ivan.AddChild(victor);
            victor.AddChild(boris);
            ivan.AddChild(peter);
            peter.AddChild(vlad);

            List<Person> descendants = ivan.FindDescendantsRecursive(Genders.Male, 1, null).ToList();
            Assert.IsTrue(descendants.Count == 2);
            Assert.AreEqual(victor, descendants[0]);
            Assert.AreEqual(peter, descendants[1]);

            descendants = ivan.FindDescendantsRecursive(Genders.Female, 1, null).ToList();
            Assert.IsTrue(descendants.Count == 1);
            Assert.AreEqual(anastasya, descendants[0]);

            descendants = ivan.FindDescendantsRecursive(Genders.Male, 2, null).ToList();
            Assert.IsTrue(descendants.Count == 2);
            Assert.AreEqual(boris, descendants[0]);
            Assert.AreEqual(vlad, descendants[1]);

            descendants = ivan.FindDescendantsRecursive(Genders.Female, 2, null).ToList();
            Assert.IsTrue(descendants.Count == 0);

            descendants = ivan.FindDescendantsRecursive(Genders.Male, 2, new List<Person>(new Person[] { ivan, victor })).ToList();
            Assert.IsTrue(descendants.Count == 1);
            Assert.AreEqual(vlad, descendants[0]);
        }

        [TestMethod()]
        public void GetBirthyearSuspiciousnessTest()
        {
            Person ivan = new Person("Иван Иванович", Genders.Male, 1970);

            Person anastasya = new Person("Анастасия Ивановна", Genders.Female, 1990);
            ivan.AddChild(anastasya);
            Assert.IsTrue(anastasya.BirthyearDataSuspiciousness == 0);

            Person victorya = new Person("Виктория Ивановна", Genders.Female, 1970);
            ivan.AddChild(victorya);
            Assert.IsTrue(victorya.BirthyearDataSuspiciousness == 2);

            Person claudya = new Person("Клавдия Ивановна", Genders.Female, 1960);
            ivan.AddChild(claudya);
            Assert.IsTrue(claudya.BirthyearDataSuspiciousness == 2);

            Person anna = new Person("Анна Ивановна", Genders.Female, 1975);
            ivan.AddChild(anna);
            Assert.IsTrue(anna.BirthyearDataSuspiciousness == 1);

            Person gennady = new Person("Геннадий Иванович", Genders.Male, 1979);//value boundaries check (9 years)
            ivan.AddChild(gennady);
            Assert.IsTrue(gennady.BirthyearDataSuspiciousness == 1);

            Person vlad = new Person("Владимир Иванович", Genders.Male, 1980);////value boundaries check (10 years)
            ivan.AddChild(vlad);
            Assert.IsTrue(vlad.BirthyearDataSuspiciousness == 0);
        }

        [TestMethod()]
        public void CheckIsGenderCorrectTest()
        {
            Person petr = new Person("Петр Иванович", Genders.Male, 1970);
            Assert.IsTrue(petr.IsGenderCorrect);
            Person petr2 = new Person("Петр Иванович", Genders.Female, 1970);
            Assert.IsFalse(petr2.IsGenderCorrect);

            Person anastasya = new Person("Анастасия Петровна", Genders.Female, 1990);
            Assert.IsTrue(anastasya.IsGenderCorrect);
            Person anastasya2 = new Person("Анастасия Петровна", Genders.Male, 1990);
            Assert.IsFalse(anastasya2.IsGenderCorrect);
        }
    }
}