﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AshTree;
using System;
using System.Collections.Generic;
using System.Text;
using NuGet.Frameworks;
using System.Linq;

namespace AshTree.Tests
{
    [TestClass()]
    public class PersonParserTests
    {
        [TestMethod()]
        public void ParseTest()
        {
            string input1 =
@"Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | 1758
	Николай Иванович | М | 1757
		Иван Николаевич | М | 1780
			Иван Иванович | М | 1805
				Мария Ивановна | Ж | 1828
			Никифор Иванович | М | 1812
		Афанасий Николаевич | М | 1784
			Яков Афанасиевич | М | 1783
			Павел Афанасиевич | М | 1805
				Анастасия Павловна | Ж | 1826
			Екатерина Афанасиевна | Ж | 1793
Артур Григорьевич | М | 1800";

			(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input1);
			Assert.IsTrue(roots.Count == 2);
			Assert.IsTrue(roots[1].Name == "Артур Григорьевич");
			Assert.IsTrue(mainIndex.Count == 16);

			Person nick;
			Assert.IsTrue(mainIndex.TryGetValue("Николай Иванович", out nick));

			Person afanasy;
			Assert.IsTrue(mainIndex.TryGetValue("Афанасий Николаевич", out afanasy));
			Assert.IsTrue(afanasy.Gender == Genders.Male);
			Assert.IsTrue(afanasy.Birthyear == 1784);
			
			Assert.AreEqual(nick, afanasy.Parent);

			Person mary;
			Assert.IsTrue(mainIndex.TryGetValue("Мария Ивановна", out mary));

			Person ivanivan;
			Assert.IsTrue(mainIndex.TryGetValue("Иван Иванович", out ivanivan));
			
			Assert.IsTrue(ivanivan.Children.Count == 1);
			Assert.AreEqual(ivanivan.Children[0], mary);
			
			List<Person> relatives = ivanivan.FindRelatives(Genders.Male, 1).ToList();
			Assert.IsTrue(relatives.Count == 1);
			Assert.IsTrue(relatives[0].Name == "Никифор Иванович");

			relatives = ivanivan.FindRelatives(Genders.Female, 1).ToList();
			Assert.IsTrue(relatives.Count == 0);

			relatives = ivanivan.FindRelatives(Genders.Female, 2).ToList();
			Assert.IsTrue(relatives.Count == 1);
			Assert.IsTrue(relatives[0].Name == "Екатерина Афанасиевна");

			relatives = ivanivan.FindRelatives(Genders.Male, 2).ToList();
			Assert.IsTrue(relatives.Count == 2);
			Assert.IsTrue(relatives[0].Name == "Яков Афанасиевич");
			Assert.IsTrue(relatives[1].Name == "Павел Афанасиевич");


			relatives = mary.FindRelatives(Genders.Male, 1).ToList();
			Assert.IsTrue(relatives.Count == 0);

			relatives = mary.FindRelatives(Genders.Male, 2).ToList();
			Assert.IsTrue(relatives.Count == 0);

			relatives = mary.FindRelatives(Genders.Female, 2).ToList();
			Assert.IsTrue(relatives.Count == 0);

			relatives = mary.FindRelatives(Genders.Female, 3).ToList();
			Assert.IsTrue(relatives.Count == 1);
			Assert.IsTrue(relatives[0].Name == "Анастасия Павловна");

			Person anna;
			Assert.IsTrue(mainIndex.TryGetValue("Анна Петрова", out anna));
			Assert.IsFalse(anna.IsGenderCorrect);
		}

		[TestMethod()]
		public void ParseTestLeadingTabException()
		{
			string input =
@"Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | 1758";

			(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input);
			Assert.IsTrue(roots.Count == 1);
			Assert.IsTrue(mainIndex.Count == 5);

			string input2 =
@"	Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | 1758";
			FormatException fex = null;
			try
			{
				(mainIndex, roots) = PersonParser.Parse(input2);
			}
			catch (FormatException ex)
			{
				fex = ex;
			}
			Assert.IsNotNull(fex);
			Assert.IsTrue(fex.Message == "Ошибка в данных: нарушена иерархия; строка 0");
		}

		[TestMethod()]
		public void ParseTestFieldsCountException()
		{
			string input =
@"Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М
	Дарья Ивановна | Ж | 1758";
			FormatException fex = null;
			try
			{
				(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input);
			}
			catch (FormatException ex)
			{
				fex = ex;
			}
			Assert.IsNotNull(fex);
			Assert.IsTrue(fex.Message == "Ошибка в данных: задано неверное количество полей; строка 3");
		}

		[TestMethod()]
		public void ParseTestHierarchyException()
		{
			string input =
@"Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
			Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | 1758";
			FormatException fex = null;
			try
			{
				(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input);
			}
			catch (FormatException ex)
			{
				fex = ex;
			}
			Assert.IsNotNull(fex);
			Assert.IsTrue(fex.Message == "Ошибка в данных: нарушена иерархия; строка 2");
		}

		[TestMethod()]
		public void ParseTestGenderException()
		{
			string input = @"Иван Андреевич | AМ | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | 1758";
			FormatException fex = null;
			try
			{
				(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input);
			}
			catch (FormatException ex)
			{
				fex = ex;

			}
			Assert.IsNotNull(fex);
			Assert.IsTrue(fex.Message == "Ошибка в данных: неверный формат данных поля №2 (пол); строка 0");
		}

		[TestMethod()]
		public void ParseTestYearException()
		{
			string input = @"Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | 1758t";
			FormatException fex = null;
			try
			{
				(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input);
			}
			catch (FormatException ex)
			{
				fex = ex;
			}
			Assert.IsNotNull(fex);
			Assert.IsTrue(fex.Message == "Ошибка в данных: неверный формат данных поля №3 (год рождения); строка 4");
		}

		[TestMethod()]
		public void ParseTestYearException2()
		{
			string input = @"Иван Андреевич | М | 1738
	Петр Иванович | М | 1756
		Глафира Петровна | Ж | 1777
		Анна Петрова | М | 1780
	Дарья Ивановна | Ж | ";
			FormatException fex = null;
			try
			{
				(Dictionary<string, Person> mainIndex, List<Person> roots) = PersonParser.Parse(input);
			}
			catch (FormatException ex)
			{
				fex = ex;
			}
			Assert.IsNotNull(fex);
			Assert.IsTrue(fex.Message == "Ошибка в данных: неверный формат данных поля №3 (год рождения); строка 4");
		}
	}
}