﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace AshTree
{

    public class Person
    {
        public Person(string name, Genders gender, int birthyear)
        {
            Name = name;
            Gender = gender;
            Birthyear = birthyear;
            IsGenderCorrect = CheckIsGenderCorrect(name, gender);
        }

        public string Name { get; private set; }

        public Genders Gender { get; private set; }

        public int Birthyear { get; private set; }

        public Person Parent { get; set; } = null;

        public int BirthyearDataSuspiciousness { get; set; } = 0;

        public bool IsGenderCorrect { get; set; } = true;

        public List<Person> Children { get; set; } = new List<Person>();

        public void AddChild(Person person)
        {
            person.Parent = this;
            Children.Add(person);
            person.BirthyearDataSuspiciousness = GetBirthyearSuspiciousness(person);
        }


        /// <summary>
        /// Finds all relatives of the specified gender by the specified level of kinship
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="level">level of kinship</param>
        /// <returns>If nothing is found returns an empty collection</returns>
        public IEnumerable<Person> FindRelatives(Genders gender, int level)
        {
            Person ancestor = this;
            List<Person> directLine = new List<Person>();
            directLine.Add(this);
            for (int i = 0; i < level; i++)
                if (ancestor.Parent != null)
                {
                    ancestor = ancestor.Parent;
                    directLine.Add(ancestor);
                }
                else
                    break;
            if (ancestor != null)
                return ancestor.FindDescendantsRecursive(gender, level, directLine);
            else
                return new Person[0];
        }

        /// <summary>
        /// Finds all descendants of given gender on given level, ignoring nodes specified in exceptionLine
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="level"></param>
        /// <param name="exceptionLine">specifies which node should be ignored on each level; index of element corresponds to level</param>
        /// <returns>If nothing is found returns an empty collection</returns>
        public IEnumerable<Person> FindDescendantsRecursive(Genders gender, int level, List<Person> exceptionLine)
        {
            if (level == 0)
            {
                if (Gender == gender)
                    yield return this;
            }
            else
            {
                foreach (Person child in Children)
                    if (child != exceptionLine?[level - 1])
                        foreach (Person sub in child.FindDescendantsRecursive(gender, level - 1, exceptionLine))
                            yield return sub;
            }

        }

        /// <summary>
        /// Gives an estimate of how suspecious the birth year is regarding to birth year of the parent
        /// </summary>
        /// <param name="person"></param>
        /// <returns>
        /// 0 if there is no big doubt
        /// 1 if there is a doubt due to biological characteristics
        /// 2 if there is violation of causation
        /// </returns>
        public static int GetBirthyearSuspiciousness(Person person)
        {
            if (person.Parent == null)
                return 0;
            int delta = person.Birthyear - person.Parent.Birthyear;
            if (delta > 9)
                return 0;
            if (delta <= 0)
                return 2;
            return 1;
        }

        /// <summary>
        /// Checks if gender matches the name ending
        /// </summary>
        /// <param name="name"></param>
        /// <param name="gender"></param>
        /// <returns></returns>
        public static bool CheckIsGenderCorrect(string name, Genders gender)
        {
            return gender == Genders.Male && name[name.Length - 1] == 'ч' || gender == Genders.Female && name[name.Length - 1] == 'а';
        }
    }
}
