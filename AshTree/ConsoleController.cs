﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AshTree
{
    class ConsoleController
    {
        Dictionary<string, Person> mainIndex;
        List<Person> roots;

        public ConsoleController(Dictionary<string, Person> mainIndex, List<Person> roots)
        {
            this.mainIndex = mainIndex;
            this.roots = roots;
        }

        public void HandleConsoleInput()
        {
            Console.WriteLine("Пример запроса: '[имя] Иван Иванович, [тип родства] Б, [степень родства] 2'");
            Console.WriteLine("'q', либо 'в' - выход");
            Console.WriteLine("'t', либо 'д' - показать дерево");
            Console.WriteLine("\t\t(красным будут отмечены даты, нарушающие причинно-следственные связи");
            Console.WriteLine("\t\tжелтым - сомнительные с биологической точки зрения)");
            Console.WriteLine("\t\tкроме того, красным будут отмечены неверные данные о поле");
            while (true)
            {
                string line = Console.ReadLine();
                if (line == "q" || line == "в")
                    break;
                if (line == "t" || line == "д")
                {
                    Console.WriteLine();
                    foreach(Person person in roots)
                        DisplayTree(person, 0);
                }
                else
                {
                    try
                    {
                        string[] parts = line.Split(',');
                        if (parts.Length != 3)
                            throw new ArgumentException("Внесены неверные аргументы");
                        //  throw new ArgumentException("Invalid arguments");

                        string name = parts[0].Replace("имя", "").Trim();

                        string kinshipType = parts[1].Replace("тип родства", "").Trim();
                        Genders kinshipTypeGender;
                        if (kinshipType.ToUpper() == "Б")
                            kinshipTypeGender = Genders.Male;
                        else if (kinshipType.ToUpper() == "С")
                            kinshipTypeGender = Genders.Female;
                        else
                            throw new ArgumentException("Неверный аргумент №2 'тип родства'");
                        //  throw new ArgumentException("Invalid format of argument 'тип родства'");

                        string kinshipLevelStr = parts[2].Replace("степень родства", "").Trim();
                        int kinshipLevel;
                        if (!int.TryParse(kinshipLevelStr, out kinshipLevel))
                            throw new ArgumentException("Неверный аргумент №3 'степень родства'");
                        //    throw new ArgumentException("Invalid format of argument 'степень родства'");

                        Person person;
                        if (mainIndex.TryGetValue(name, out person))
                        {
                            bool gotEntries = false;
                            foreach (Person relative in person.FindRelatives(kinshipTypeGender, kinshipLevel))
                            {
                                if (gotEntries)
                                    Console.Write(", ");
                                else
                                    gotEntries = true;

                                Console.Write(relative.Name);
                            }
                            if (!gotEntries)
                                Console.WriteLine("Указанные родственные связи не найдены");
                            Console.WriteLine();
                        }
                        else
                            throw new ArgumentException("Человек с заданным именем не найден");
                        //    throw new ArgumentException("Person not found");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }

        }


        public void DisplayTree(Person person, int level)
        {
            for (int i = 0; i < level; i++)
                Console.Write("  ");
            Console.Write(person.Name);
            Console.Write("  ");
            if (!person.IsGenderCorrect)
                Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(person.Gender);
            
            Console.Write("  ");
            
            if (person.BirthyearDataSuspiciousness == 1)//most likely invalid birthyear due to biological characteristics will be marked yellow
                Console.ForegroundColor = ConsoleColor.Yellow;
            else if (person.BirthyearDataSuspiciousness == 2)//violation of causation will be marked red
                Console.ForegroundColor = ConsoleColor.Red;
            else
                Console.ResetColor();

            Console.Write(person.Birthyear);

            Console.ResetColor();

            Console.WriteLine();

            foreach (Person child in person.Children)
                DisplayTree(child, level+1);//recursively displaying data about children 
        }

    }
}
