﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace AshTree
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "input.txt";
            if (args.Length > 0)
                filename = args[0];

            (var mainIndex, var roots) = PersonParser.FromFile(filename);
            ConsoleController consoleController = new ConsoleController(mainIndex, roots);
            consoleController.HandleConsoleInput();
        }
    }
}
