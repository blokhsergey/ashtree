﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace AshTree
{
    public static class PersonParser
    {
        public static (Dictionary<string, Person> index, List<Person> roots) FromFile(string file)
        {
            return ParseLines(File.ReadAllLines(file));
        }

        public static (Dictionary<string, Person> index, List<Person> roots) Parse(string familyTreeString)
        {
            return ParseLines(familyTreeString.Split(Environment.NewLine));
        }

        private static (Dictionary<string, Person> index, List<Person> roots) ParseLines(string[] familyTreeLines)
        {
            Dictionary<string, Person> mainIndex = new Dictionary<string, Person>();
            List<Person> roots = new List<Person>();
            Person parent = null;
            Person previous = null;
            int currentLevel = 0;
            if (familyTreeLines[0][0]=='\t')
                throw new FormatException("Ошибка в данных: нарушена иерархия; строка 0");
            //  throw new FormatException($"Invalid input file format - first line can't start with Tab character; line 0");

            int lineIndex = 0;
            foreach(string line in familyTreeLines)
            {
                string[] parts = line.Split('|');

                if (parts.Length != 3)
                    throw new FormatException($"Ошибка в данных: задано неверное количество полей; строка {lineIndex}");
                //  throw new FormatException($"Invalid input file format - wrong number of fields specified; line {lineIndex}");

                string name = parts[0];

                int level = name.TakeWhile(c => c == '\t').Count();
                int levelDelta = level - currentLevel;
                
                if (levelDelta > 1)//jump over child to grandchild
                    throw new FormatException($"Ошибка в данных: нарушена иерархия; строка {lineIndex}");
                //  throw new FormatException($"Invalid relationship specified in input file; line {lineIndex}");

                if (levelDelta == 1)//child
                    parent = previous;
                else if(levelDelta < 0)//one of the ancestors should be set as parent
                {
                    for (int i = 0; i < Math.Abs(levelDelta); i++)
                        parent = parent.Parent;
                }
                //if levelDelta == 0 (sib) nothing needs to be done

                currentLevel = level;

                name = name.Trim();

                Genders gender;
                switch(parts[1].Trim())
                {
                    case "М": gender = Genders.Male; break;
                    case "Ж": gender = Genders.Female; break;
                    default: throw new FormatException($"Ошибка в данных: неверный формат данных поля №2 (пол); строка {lineIndex}");
                    //default: throw new FormatException($"Invalid value for field 2 'Gender' in input file; line {lineIndex}");
                }
                
                int birthYear;
                if (!int.TryParse(parts[2].Trim(), out birthYear))
                    throw new FormatException($"Ошибка в данных: неверный формат данных поля №3 (год рождения); строка {lineIndex}");
                //  throw new FormatException($"Invalid value for field 3 'BirthYear' in input file; line {lineIndex}");

                Person newPerson = new Person(name, gender, birthYear);

                if (parent != null)
                    parent.AddChild(newPerson);

                mainIndex.Add(name, newPerson);
                if (level == 0)
                    roots.Add(newPerson);
                previous = newPerson;
                lineIndex++;
            }

            return (mainIndex, roots);
        }
    }
}
